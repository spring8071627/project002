package com.example.demo.di;

import org.springframework.stereotype.Component;

@Component
public class Cafe {

	Manager manager;
	
	public Manager getManager() {
		return manager;
	}
	
}
