package com.example.demo.di;

import org.springframework.stereotype.Component;

@Component
public class Cat {

	public void Eat() {
		System.out.println("쥐를 먹는다.");
	}
}
