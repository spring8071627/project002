/*
 * 1.자동차클래스(Car)을 설계한다
 * 멤버변수: 모델명, 제조사, 색
 * 기능: 
 * 모든멤버변수의 getter/setter, 
 * 디폴트생성자, 
 * 모든멤버변수를 초기화하는 생성자, 
 * 도서정보를 반환하는 기능
 *
 * 2.자동차 인스턴스를 생성하고, 자동차 클래스가 가진 기능을 테스트한다
 * */

package com.example.demo.lombok;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Car {

	String model;
	String company;
	String color;

}
