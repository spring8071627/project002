package com.example.demo.lombok;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

// @data 는 게터 세터 투스트링 3개를 모두 가지고있다.
@Getter
@Setter
@ToString
@NoArgsConstructor // 생성자
@AllArgsConstructor // 생성자


public class Person {
	
	String name;
	int age;
}
