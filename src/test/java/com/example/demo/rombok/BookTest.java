package com.example.demo.rombok;

import org.junit.jupiter.api.Test;

import com.example.demo.lombok.Book;

public class BookTest {

	@Test 
	void test() {
		Book book1 = new Book();
		book1.setCompany("한빛미디어");
		book1.setPage(800);
		book1.setPrice(22000);
		book1.setTitle("자바프로그래밍");
		
		System.out.println(book1.getCompany());
		System.out.println(book1.getPage());
		System.out.println(book1.getPrice());
		System.out.println(book1.getTitle());
		
		System.out.println(book1);
	}
}
