package com.example.demo.rombok;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.lombok.Person;

@SpringBootTest // 단위테스트
public class PersonTest {

	@Test // 단위테스트
	void test() {
		Person person1 = new Person();
		person1.setName("둘리");
		person1.setAge(12);
		System.out.println(person1.getName());
		System.out.println(person1.getAge());
		
		Person person2 = new Person("또치", 15);
		System.out.println(person2.toString());
	}
}
