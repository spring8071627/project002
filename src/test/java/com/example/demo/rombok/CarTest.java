package com.example.demo.rombok;

import org.junit.jupiter.api.Test;

import com.example.demo.lombok.Car;

public class CarTest {

	@Test
	void test() {
		Car car1 = new Car();
		car1.setModel("소나타");
		car1.setCompany("현대");
		car1.setColor("블랙");
		System.out.println(car1.getModel());
		System.out.println(car1.getCompany());
		System.out.println(car1.getColor());
		
		Car car2 = new Car("베뉴","현대","그레이");
		System.out.println(car2);
	
}
}
