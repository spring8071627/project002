package com.example.demo.di;

import static org.mockito.ArgumentMatchers.anyByte;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ManagerTest {

	@Autowired
	Cafe cafe;
	
	@Autowired
	Manager manager;
	@Test
	void test() {
		System.out.println("Cafe : " + cafe);
		System.out.println("Manager : " + manager);
		System.out.println("Cafe : " + cafe.getManager());
		
	}
}
