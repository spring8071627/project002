package com.example.demo.di;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TeacherTest {

	@Autowired
	Classs classs;
	
	@Autowired
	Teacher teacher;
	
	@Test
	void test() {
		System.out.println("classs : " + classs);
		System.out.println("teacher : " + teacher);
		System.out.println("classs : " + teacher.getClasss());
	}
}
